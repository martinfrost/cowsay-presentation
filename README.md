These are my slides from a talk I did at STHLM-VIM 2015-03-09

I used my own [cowerpoint](https://github.com/Frost/cowerpoint) script
to view the slides, like so:

    cowerpoint ./slides
